
#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;
using namespace Catch::Matchers;

string full_name(const string& first, const string& last)
{
    return first + " " + last;
}

TEST_CASE("reference binding")
{
    string name = "jan";

    SECTION("C++98")
    {
        string& ref_name = name;

        const string& ref_full_name = full_name(name, "kowalski");
        //ref_full_name[0] = 'J'; // ERROR
    }

    SECTION("C++11")
    {
        string&& rref_full_name = full_name(name, "nowak");
        rref_full_name[0] = 'J';

        //string&& rref_name = name; // ERROR - illegal binding
    }
}

TEST_CASE("state after move")
{
    SECTION("destruction after is safe")
    {
        string name = "jan";
        string target = move(name);
        REQUIRE(target == "jan"s);
    }

    SECTION("assignment is safe")
    {
        string name = "jan";
        string target = move(name);
        REQUIRE(target == "jan"s);
        
        name = string("adam"); // is safe
        REQUIRE(name == "adam"s);
    }

    SECTION("use after move is Undefined Behavoir")
    {
        string name = "jan";
        string target = move(name);
        auto length = name.length();
    }
}

class Gadget
{
    int id_;
    std::string name_;
public:
    Gadget(int id, const std::string& name) : id_{id}, name_{name}
    {
        std::cout << "Gadget(" << id_ << " " << name_ << ")\n";
    }

    ~Gadget()
    {
        std::cout << "~Gadget(" << id_ << " " << name_ << ")\n";
    }

    void use() const
    {
        std::cout << "Using Gadget(" << id_ << ", " << name_ << ")\n";
    }
};

void may_throw(int arg)
{
    if (arg == 13)
        throw std::runtime_error("Error#13");
}

namespace LegacyCode
{
    Gadget* create_gadget();

    Gadget* create_gadget()
    {
        return new Gadget{42, "tablet"};
    }

    void use1(Gadget* g);

    void use1(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use2(Gadget* g);

    void use2(Gadget* g)
    {
        if (g)
            g->use();

        delete g;
    }
}

// TEST_CASE("memory leaks")
// {
//     Gadget* ptr_g = new Gadget(1, "ipad");
//     ptr_g->use();

//     Gadget* ptr_g2 = LegacyCode::create_gadget();

//     use1(ptr_g);
//     use2(ptr_g2);

//     may_throw(14);
    
//     delete ptr_g;
//     //delete ptr_g2;
// }

template <typename T>
class UniquePtr
{
    T* ptr_ = nullptr;
public:
    explicit UniquePtr(T* ptr) : ptr_{ptr}
    {}   

    // UniquePtr is non-copyable
    UniquePtr(const UniquePtr&) = delete; 
    UniquePtr& operator=(const UniquePtr&) = delete;    

    // move constructor
    UniquePtr(UniquePtr&& other) : ptr_{other.ptr_}
    {
        other.ptr_ = nullptr;
    }

    // move assignment
    UniquePtr& operator=(UniquePtr&& other)
    {
        if (this != &other)
        {
            delete ptr_;

            ptr_ = other.ptr_;
            other.ptr_ = nullptr;
        }

        return *this;
    }

    ~UniquePtr()
    {
        delete ptr_;
    }

    operator bool() 
    {
        return ptr_ != nullptr;
    }

    T& operator*() const
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    T* get() const
    {
        return ptr_;
    }
};

TEST_CASE("std::move")
{
    int x = 10;
    int y = std::move(x); // it does nothing

    std::string str = "text";
    std::string other = std::move(str);
    REQUIRE(other == "text"s);
    REQUIRE(str == ""s);
}

namespace ModernCpp
{
    UniquePtr<Gadget> create_gadget();

    UniquePtr<Gadget> create_gadget()
    {
        static int id = 1000;
        return UniquePtr<Gadget>(new Gadget{++id, "Gadget"});
    }

    void use1(Gadget* g);

    void use1(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use2(UniquePtr<Gadget> g);

    void use2(UniquePtr<Gadget> g)
    {
        if (g)
            g->use();        
    }

    UniquePtr<Gadget> use3(UniquePtr<Gadget> g)
    {
        if (g)
            g->use();

        return g;
    }
}

TEST_CASE("safe code")
{    
    UniquePtr<Gadget> ptr_g = ModernCpp::create_gadget(); // Resource Acquisition
    ptr_g->use();

    ModernCpp::use1(ptr_g.get());

    SECTION("move constructor")
    {
        UniquePtr<Gadget> ptr_other = std::move(ptr_g); // call of move constructor

        ptr_other->use();    
        REQUIRE(ptr_g.get() == nullptr);
    }

    SECTION("move assignment")
    {
        ptr_g = UniquePtr<Gadget>(new Gadget{665, "smartphone"}); // call move operator=
    }

    SECTION("passing unique_ptr to function")
    {
        UniquePtr<Gadget> ptr_watch(new Gadget{88, "watch"});
        ModernCpp::use2(std::move(ptr_watch)); // ptr_watch is lvalue -> std::move is required to pass by value
        ModernCpp::use2(ModernCpp::create_gadget()); // ModernCpp::create_gadget() is rvalue -> move is implicit        
        
        ptr_watch = ModernCpp::create_gadget();

        auto ptr_other =  ModernCpp::use3(std::move(ptr_watch));
        ptr_other->use();
    }
} // Release of resource

void have_fun(const Gadget& g)
{
    puts(__PRETTY_FUNCTION__);
    g.use();
}

void have_fun(Gadget& g)
{
    puts(__PRETTY_FUNCTION__);
    g.use();
}

void have_fun(Gadget&& g)
{
    puts(__PRETTY_FUNCTION__);
    g.use();
}

// void use(Gadget& g)
// {
//     have_fun(g);
// }

// void use(const Gadget& g)
// {
//     have_fun(g);
// }

// void use(Gadget&& g)
// {
//     have_fun(std::move(g));
// }

template <typename T>
void use(T&& g)
{
    have_fun(std::forward<T>(g));
}

TEST_CASE("Perfect forwarding")
{
    cout << "\n--Perfect forwarding--\n\n";
    Gadget g{1, "ipad"};
    const Gadget cg{2, "const ipad"};

    use(cg); 
    use(g);
    use(Gadget{3, "temp gadget"});   
}

template <typename T>
void deduce1(T obj)
{}

template <typename T>
void deduce2(T&& obj)
{}

TEST_CASE("auto&& - universal reference")
{
    auto x = 1; // int
    deduce1(1); // deduce<int>(int)

    auto text = "abc"; // const char*
    deduce1("abc"); // deduce<const char*>

    auto&& ax = 1; // int&& == deduce2<int>(int&&)
    auto&& bx = x; // int& == deduce2<int&>(int& &&) -> deduce2<int&>(int&)
}


namespace OldStyleVariadicTemplates
{
    template <typename T>
    UniquePtr<T> make_UniquePtr()
    {
        return UniquePtr<T>(new T{});
    }

    template <typename T, typename Arg1>
    UniquePtr<T> make_UniquePtr(Arg1&& arg1)
    {
        return UniquePtr<T>(new T{std::forward<Arg1>(arg1));
    }

    template <typename T, typename Arg1, typename Arg2>
    UniquePtr<T> make_UniquePtr(Arg1&& arg1, Arg2&& arg2)
    {
        return UniquePtr<T>(new T{std::forward<Arg1>(arg1), std::forward<Arg2>(arg2)});
    }
}

template <typename T, typename... Arg>
UniquePtr<T> make_UniquePtr(Arg&&... arg)
{
    return UniquePtr<T>(new T{std::forward<Arg>(arg)...});
}

TEST_CASE("practical perfect forwarding")
{    
    UniquePtr<Gadget> ptr_g = make_UniquePtr<Gadget>(887, "ipad");
    ptr_g->use();

    UniquePtr<vector<int>> ptr_vec = make_UniquePtr<vector<int>>();
    REQUIRE(ptr_vec->size() == 0);

    vector<Gadget> gadgets;

    gadgets.push_back(Gadget{1, "g1"});
    gadgets.push_back(Gadget{2, "g2"});
    gadgets.push_back(Gadget{3, "g3"});
    gadgets.emplace_back(4, "g4");
}

template <typename T>
class MyVector
{
    std::vector<T> items_;
public:
    void push_back(const T& obj)
    {
        items_.push_back(obj);
    }

    void push_back(T&& obj) // r-value reference
    {
        items_.push_back(std::move(obj));
    }

    template <typename... Args>
    void emplace_back(Args&&... args) // forwarding reference
    {
        items_.emplace_back(std::forward<Args>(args)...);
    }

    size_t size() const
    {
        return items_.size();
    }

    T& operator[](size_t index) 
    {
        return items_[index];
    }
};

TEST_CASE("using MyVector")
{
    MyVector<Gadget> gadgets_ptrs;
    
    gadgets_ptrs.emplace(42, "g42");   

    gadgets_ptrs[0].use();
}

TEST_CASE("MyVector of std::unique_ptr")
{
    MyVector<std::unique_ptr<Gadget>> gadgets_ptrs;

    gadgets_ptrs.push_back(std::make_unique<Gadget>(43, "g43"));
    gadgets_ptrs.emplace_back(std::make_unique<Gadget>(665, "g665"));
    //gadgets_ptrs.emplace_back(new Gadget(66, "g66")); // ERROR-prone style
}