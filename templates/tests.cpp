#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <type_traits>
#include <atomic>
#include <utility>
#include <array>

using namespace std;

template <typename T>
const T& maximum(const T& a, const T& b)
{
    return a < b ? b : a;
}

const char* maximum( const char* a, const char* b)
{
    return strcmp(a, b) < 0 ? b : a;
}

TEST_CASE("function template")
{
    auto m1 = maximum(1, 2); // maximum<int>(int, int)
    REQUIRE(m1 == 2);

    REQUIRE(maximum("abc"s, "def"s) == "def"s); // maximum<string>(const string&, const string&)

    auto m2 = maximum<double>(1, 3.14);
    REQUIRE(m2 == 3.14);

    auto m3 = maximum("abc", "def"); // maximum<const char*>(const char*, const char*)
    REQUIRE(m3 == "def"s);
}

template <typename TResult, typename T1, typename T2>
TResult add(T1 a, T2 b)
{
    return a + b;
}

namespace SinceCpp14
{
    template <typename T1, typename T2>
    auto add(T1 a, T2 b)
    {
        return a + b;
    }
}

template <typename T1, typename T2>
std::common_type_t<T1, T2> max_value(T1 a, T2 b)
{
    return a < b ? b : a;
}

TEST_CASE("return type")
{
    auto sum = add<double>(1, 3.14);
    REQUIRE(sum == Approx(4.14));

    REQUIRE(SinceCpp14::add(1, 3.14) == Approx(4.14));

    REQUIRE(max_value(1, 3.14) == Approx(3.14));
}

template <typename T1, typename T2>
std::vector<std::common_type_t<T1, T2>> create_vector_2(T1 a, T2 b)
{
    using VectorCT = std::vector<std::common_type_t<T1, T2>>;
    VectorCT vec;
    vec.push_back(std::move(a));
    vec.push_back(std::move(b));
    return vec;
}

TEST_CASE("traits & common_type")
{
    auto vec = create_vector_2(1, 3.14);

    REQUIRE(vec[1] == Approx(3.14));
}

///////////////////////////////////////////////
// class templates

struct InPlaceT
{};

constexpr InPlaceT my_in_place;

template <typename T>
class Holder
{
    T item_;
public:
    using value_type = T;

    Holder(const T& item) : item_{item}
    {}

    Holder(T&& item) : item_{std::move(item)}
    {}

    template <typename ItemT>
    Holder(std::in_place_t, ItemT&& item) 
        : item_(std::forward<ItemT>(item))
    {        
    }

    T& value()
    {
        return item_;
    }

    const T& value() const
    {
        return item_;
    }

    void info() const 
    {
        std::cout << "Holder<T: " << typeid(T).name() << ">(" << item_ << ")\n";
    }
};

template <typename T>
class Holder<T*>
{
    T* ptr_;
public:
    using value_type = T;

    explicit Holder(T* ptr) : ptr_{ptr}
    {}

    Holder(const Holder&) = delete;
    Holder& operator=(const Holder&) = delete;

    Holder(Holder&& other) : ptr_{other.ptr_}
    {
        other.ptr_ = nullptr;
    }

    Holder& operator=(Holder&& other) 
    {
        if (this != &other)
        {
            this->~Holder();

            ptr_ = other.ptr_;
            other.ptr_ = nullptr;
        }

        return *this;
    }


    ~Holder() 
    {
        delete[] ptr_;
    }
    
    T& value() const
    {
        return *ptr_;
    }

    T* get() const
    {
        return ptr_;
    }

    void info() const 
    {
        std::cout << "Holder<T*: " << typeid(T).name() << ">(" << *ptr_ << ")\n";
    }
};

template <>
class Holder<const char*>
{
    const char* text_;
public:
    using value_type = const char*;

    Holder(const char* text) : text_{text}
    {}

    std::string_view value() const
    {
        return text_;
    }

    void info() const
    {
        cout << "Holder<const char*>(" << text_ << ")" << endl;
    }
};

TEST_CASE("Holder holds values")
{
    Holder<int> h1{42};

    REQUIRE(h1.value() == 42);

    h1.info();

    Holder<std::string> h2{"text"};

    Holder<std::atomic<int>> h3{in_place, 665};

    SECTION("holding pointers")
    {
        Holder<int*> hp1{new int(13)};

        REQUIRE(hp1.value() == 13);

        hp1.info();

        std::cout << hp1.get() << "\n";
    }

    SECTION("holding c-strings")
    {
        Holder<const char*> hcs("text");

        hcs.info();
        std::cout << hcs.value() << "\n";                
    }
}

TEST_CASE("std::array")
{
    std::array<int, 10> arr{};

    arr[3] = 44;

    REQUIRE(arr.size() == 10);
}