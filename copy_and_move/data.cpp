#include "catch.hpp"
#include <iostream>

using namespace std::literals;

class Data
{
    std::string name_ {};
    std::string* data_;
    size_t size_;

public:
    friend std::ostream& operator<<(std::ostream& out, const Data& data);

    Data(std::string name, std::initializer_list<std::string> lst)
        : name_ {std::move(name)}
        , data_ {new std::string[lst.size()]}
        , size_ {lst.size()}
    {
        std::copy(lst.begin(), lst.end(), data_);

        std::cout << "Constructor " << *this << "\n";
    }    

    // copy constructor
    Data(const Data& other)
        : name_ {other.name_}
        , data_ {new std::string[other.size()]}
        , size_ {other.size()}
    {
        std::copy(other.data_, other.data_ + size_, data_);
        std::cout << "Copy Ctor " << *this << "\n";
    }

    // copy assignment
    Data& operator=(const Data& other)
    {
        Data temp {other}; // konstruktor kopiujący
        swap(temp);

        return *this;
    }

    // move constructor
    Data(Data&& other) noexcept
        : name_ {std::move(other.name_)} // noexcept
        , data_ {other.data_} // noexcept
        , size_ {other.size_} // noexcept
    {
        other.data_ = nullptr; // noexcept
        other.size_ = 0; // noexcept
        std::cout << "Move Ctor " << *this << "\n";
    }

    // move assignment
    Data& operator=(Data&& other)
    {
        if (this != &other)
        {
            this->~Data();
            
            name_ = std::move(other.name_); // it may throw
                        
            data_ = other.data_;
            size_ = other.size_;

            other.data_ = nullptr;
            other.size_ = 0;            
        }

        std::cout << "Move op= " << *this << "\n";

        return *this;
    }


    void swap(Data& other)
    {
        std::swap(name_, other.name_);
        std::swap(data_, other.data_);
        std::swap(size_, other.size_);
    }

    ~Data()
    {
        std::cout << "Destructor " << *this << "\n";

        delete[] data_;
    }

    size_t size() const
    {
        return size_;
    }

    const std::string& name() const
    {
        return name_;
    }

    std::string& operator[](size_t index)
    {
        return data_[index];
    }

    const std::string& operator[](size_t index) const
    {
        return data_[index];
    }
};

std::ostream& operator<<(std::ostream& out, const Data& data)
{
    out << "Data{ " << data.name() << ", { ";

    for (size_t i = 0; i < data.size(); ++i)
        out << data[i] << " ";

    out << "} }";

    return out;
}

TEST_CASE("Data")
{
    Data dataset {"dataset", {"a", "b", "c"}};

    REQUIRE(dataset.name() == "dataset"s);
    REQUIRE(dataset.size() == 3);
    REQUIRE(dataset[0] == "a"s);
    REQUIRE(dataset[1] == "b"s);
    REQUIRE(dataset[2] == "c"s);

    std::cout << dataset << "\n";
}

TEST_CASE("Data - making copies")
{
    Data ds {"ds1", {"a", "b", "c"}};
    Data backup = ds;

    REQUIRE(ds.name() == backup.name());
    REQUIRE(ds[0] == backup[0]);
}

TEST_CASE("Data - move")
{
    SECTION("constructor")
    {
        Data ds {"ds2", {"hello", "world", "!!!"}};

        Data other = std::move(ds);
    }

    SECTION("assignment")
    {
        std::string name = "ds3";
        Data ds {name, {"c", "d", "e"}};
        Data other{"other", {"1", "2", "3"}};

        ds = std::move(other);
        ds = Data{"ds4", {"haha", "it", "works"}};
    }
}

struct BigDataSet
{
    Data ds;
    std::vector<std::string> row_names;

    BigDataSet(Data dataset, std::vector<std::string> rows)
        : ds{std::move(dataset)}, row_names{std::move(rows)}
    {}
};

TEST_CASE("BigDataSet & move semantics")
{   
    std::cout << "\n\n---------\n";

    BigDataSet bds{Data{"bds", {"a", "b", "c"}}, {"one", "two", "three"}};

    BigDataSet target = std::move(bds);
}

TEST_CASE("Data & std::vector")
{
    std::cout << "\n\n------------------Data & std::vector-------------------\n";

    std::vector<Data> datasets;

    datasets.reserve(3);

    datasets.push_back(Data{"one", {"1", "2", "3"}});
    //datasets.emplace_back("one", std::initializer_list<std::string>{"1", "2", "3"});

    std::cout << "\n";
    datasets.push_back(Data{"two", {"1", "2", "3"}});

    std::cout << "\n";
    datasets.push_back(Data{"three", {"1", "2", "3"}});

    std::cout << "\n";
    datasets.emplace_back("four", std::initializer_list<std::string>{"1", "2", "3"});
}

//////////////////
// Only since C++17

// template <typename T>
// class MyVector
// {
// public:
//     void push_back(const T& obj)
//     {
//         if constexpr(std::is_no_throw_move_constructible_v<T>)
//         {
//             buffer_[i] = std::move(obj);
//         }
//         else
//         {
//             buffer_[i] = obj;
//         }
//     }
// };