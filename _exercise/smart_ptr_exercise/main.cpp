#include <exception>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>

using namespace std;

class Gadget
{
public:
    // konstruktor
    Gadget(int id = 0)
        : id_{id}
    {
        std::cout << "Konstruktor Gadget(" << id_ << ")\n";
    }

    // destruktor
    ~Gadget()
    {
        std::cout << "Destruktor ~Gadget(" << id_ << ")\n";
    }

    int id() const
    {
        return id_;
    }

    void set_id(int id)
    {
        id_ = id;
    }

    void use()
    {
        std::cout << "Using a gadget with id: " << id() << '\n';
    }

    void unsafe()
    {
        std::cout << "Using a gadget with id: " << id() << " - Ups... It crashed..." << std::endl;
        throw std::runtime_error("ERROR");
    }

private:
    int id_;
};

namespace LegacyCode
{
    Gadget* create_many_gadgets(unsigned int size)
    {
        Gadget* many_gadgets = new Gadget[size];

        for (unsigned int i = 0; i < size; ++i)
            many_gadgets[i].set_id(i);

        return many_gadgets;
    }
}

void reset_value(Gadget& g, int n)
{
    // some logic

    g.set_id(n);
    cout << "New id for Gadget: " << g.id() << endl;
}

//////////////////////////////////////////////
// TODO - modernize the code below

class Player
{
    std::unique_ptr<Gadget> gadget_;
    std::ostream* logger_;

    Player(const Player&);
    Player& operator=(const Player&);

public:
    Player(std::unique_ptr<Gadget> g, std::ostream* logger = NULL)
        : gadget_(std::move(g))
        , logger_(logger)
    {
        if (!g)
            throw std::invalid_argument("Gadget can not be null");
    }

    ~Player()
    {
        if (logger_)
            *logger_ << "Destroing a gadget: " << gadget_->id() << std::endl;        
    }

    void play()
    {
        if (logger_)
            *logger_ << "Player is using a gadget: " << gadget_->id() << std::endl;

        gadget_->use();
    }
};

namespace LegacyCode
{
    void free_gadget(Gadget* g)
    {
        //...
        delete g;
    }
}

void unsafe1() // TODO: poprawa z wykorzystaniem smart_ptr
{
    auto ptr_gdgt = std::make_unique<Gadget>(4);

    /* kod korzystajacy z ptrX */

    reset_value(*ptr_gdgt, 5);

    ptr_gdgt->unsafe();

    LegacyCode::free_gadget(ptr_gdgt.release());
}

void unsafe2() // TODO: poprawa z wykorzystaniem smart_ptr
{
    int size = 10;

    std::unique_ptr<Gadget[]> buffer(LegacyCode::create_many_gadgets(size));

    /* kod korzystający z buffer */

    for (int i = 0; i < size; ++i)
        buffer[0].unsafe();    
}

void unsafe3() // TODO: poprawa z wykorzystaniem smart_ptr
{
    std::vector<std::unique_ptr<Gadget>> my_gadgets;

    my_gadgets.push_back(std::make_unique<Gadget>(87));
    my_gadgets.push_back(std::make_unique<Gadget>(12));
    my_gadgets.push_back(std::make_unique<Gadget>(98));

    int value_generator = 0;
    for (const auto& g : my_gadgets)
    {
        cout << "Gadget's old id: " << g->id() << endl;
        reset_value(*g, ++value_generator);
    }

    my_gadgets[0].reset();

    Player p(std::move(my_gadgets.back()));
    my_gadgets.pop_back();
    
    p.play();

    my_gadgets[1]->unsafe();    
}

int main() try
{
    try
    {
        unsafe1();
        unsafe2();
        unsafe3();
    }
    catch (const exception& e)
    {
        cout << "Exception caught: " << e.what() << endl;
    }
}
catch (const exception& e)
{
    cout << "Exception caught: " << e.what() << endl;
}