#include <iostream>
#include <memory>

class Human
{
public:
	Human(const std::string& name) : name_(name)
    {
		std::cout << "Konstruktor Human(" << name_ << ")" << std::endl;
	}
	
	~Human()
	{
		std::cout << "Destruktor ~Human(" << name_ << ")" << std::endl;
	}
	
	void set_partner(std::shared_ptr<Human> partner)
	{
		partner_ = partner;
	}

    void description() const
    {
        std::cout << "My name is " << name_ << std::endl;

        if (auto my_partner = partner_.lock(); my_partner)
        {
            std::cout << "My partner is " << my_partner->name_ << std::endl;
        } // implicit unlock
    }

private:
	std::weak_ptr<Human> partner_;
    std::string name_;
};

void memory_leak_demo()
{
	// RC husband == 1
	auto  husband = std::make_shared<Human>("Jan");

	// RC wife == 1
	auto wife = std::make_shared<Human>("Ewa");
	
	// RC wife ==2
    husband->set_partner(wife);
	
	// RC husband == 2
    wife->set_partner(husband);

    husband->description();
}

int main()
{
    memory_leak_demo();
}
