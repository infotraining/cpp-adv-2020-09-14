#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <cstdio>
#include <cstdlib>

using namespace std;
using namespace Catch::Matchers;

class Gadget
{
    int id_;
    std::string name_;
public:
    Gadget(int id, const std::string& name) : id_{id}, name_{name}
    {
        std::cout << "Gadget(" << id_ << " " << name_ << ")\n";
    }

    ~Gadget()
    {
        std::cout << "~Gadget(" << id_ << " " << name_ << ")\n";
    }

    void use() const
    {
        std::cout << "Using Gadget(" << id_ << ", " << name_ << ")\n";
    }
};

TEST_CASE("unique_ptrs")
{
    std::vector<std::unique_ptr<Gadget>> vec;

    auto g = std::make_unique<Gadget>(3, "three");

    vec.push_back(std::make_unique<Gadget>(1, "one"));
    vec.push_back(std::make_unique<Gadget>(2, "two"));
    vec.push_back(std::move(g));

    for(const auto& g : vec)
        g->use();

    auto owner = std::move(vec[1]);    
    vec[1] = std::make_unique<Gadget>(4, "four");
    REQUIRE(vec.size() == 3);
        
    owner->use();
}

TEST_CASE("custom deallocator")
{
    std::unique_ptr<FILE, int(*)(FILE*)> unique_file{fopen("test.txt", "w+"), &fclose};

    fprintf(unique_file.get(), "abc");
}

TEST_CASE("unique_ptr & dynamic arrays")
{
    std::unique_ptr<int[]> arr{new int[1024]};
    arr[0] = 1;
    arr[1] = 12;
} // arr calls delete[] for dynamic array

TEST_CASE("shared_ptrs + weak_ptrs")
{
    std::cout << "\n\n---------- shared_ptrs ------------\n\n";

    auto root_sp = std::make_shared<Gadget>(1001, "sp controlled");
    root_sp->use();

    std::weak_ptr<Gadget> root_wp = root_sp;

    SECTION("using weak_ptr")
    {        
        if (auto local_sp = root_wp.lock(); local_sp)
        {            
            local_sp->use();
        }
    }    

    REQUIRE(root_sp.use_count() == 1);

    {
        auto sp1 = root_sp; // copy of shared_ptr
        REQUIRE(sp1.use_count() == 2);
        sp1->use();

        {
            std::shared_ptr<Gadget> sp2 = sp1; // increments ref counter
            REQUIRE(root_sp.use_count() == 3);

            std::shared_ptr<Gadget> target = std::move(sp2);
            REQUIRE(sp2 == nullptr);
            REQUIRE(root_wp.use_count() == root_sp.use_count());
            REQUIRE(root_sp.use_count() == 3);
            target->use();
        }

        REQUIRE(sp1.use_count() == 2);
    }
    REQUIRE(root_sp.use_count() == 1);

    root_sp->use();

    root_sp.reset();
    REQUIRE(root_wp.expired());

    SECTION("weak_ptr to deallocated object")
    {    
        auto local_sp = root_wp.lock();
        REQUIRE(local_sp == nullptr);

        REQUIRE_THROWS_AS(std::shared_ptr<Gadget>(root_wp), std::bad_weak_ptr);
    }

    std::cout << "END\n";
}

// enable_shared_from_this

struct Object : std::enable_shared_from_this<Object>
{
    std::shared_ptr<Object> get_ptr()
    {        
        return shared_from_this();
    }
};

TEST_CASE("enable shared from this")
{
    auto sp1 = std::make_shared<Object>();

    auto sp2 = sp1->get_ptr();

    REQUIRE(sp1.use_count() == 2);

    SECTION("get_ptr work only for object created with make_shared")
    {
        Object o;
        REQUIRE_THROWS_AS(o.get_ptr(), std::bad_weak_ptr);
    }    
}

struct Base
{
    virtual void use()
    {
        std::cout << "Base::use()\n";
    }

    virtual ~Base() = default;
};

struct Derived : Base
{
    void use() override
    {
        std::cout << "Derived::use()\n";
    }

    void derived_only()
    {
        std::cout << "Derived only\n";
    }
};

TEST_CASE("shared_ptr & casts")
{
    std::shared_ptr<Base> sp_base = std::make_shared<Derived>();
    sp_base->use();

    std::shared_ptr<Derived> sp_derived = std::dynamic_pointer_cast<Derived>(sp_base);
    sp_derived->derived_only();    

    REQUIRE(sp_base.use_count() == 2);
}